import request from '@/utils/request'

// 查询部门列表
export function listDept(query) {
  return request({
    url: '/api/group/tree',
    method: 'post',
    params: query
  })
}

// 查询部门列表（排除节点）
export function listDeptExcludeChild(deptId) {
  return request({
    url: '/system/dept/list/exclude/' + deptId,
    method: 'get'
  })
}

// 查询部门详细
export function getDept(deptId) {
  return request({
    url: '/api/group/getGroup' ,
    method: 'post',
    params: {"id":deptId.toString()}
  })
}

// 查询部门下拉树结构
export function treeselect() {
  return request({
    url: '/api/group/tree',
    method: 'post'
  })
}

// 根据角色ID查询部门树结构
export function roleDeptTreeselect(roleId) {
  return request({
    url: '/system/dept/roleDeptTreeselect/' + roleId,
    method: 'get'
  })
}

// 新增部门
export function addDept(data) {
  return request({
    url: '/api/group/save',
    method: 'post',
    data: data
  })
}

// 修改部门
export function updateDept(data) {
  return request({
    url: '/api/group/save',
    method: 'post',
    data: data
  })
}

// 删除部门
export function delDept(id) {
	  var ids=[];
	  if((typeof id=='string') && id.constructor==String){
		  ids.push(id);
	  }else{
		  ids=id;
	  }
	  return request({
	    url: '/api/group/batchDelete',
	    method: 'post',
	    data:ids
	  })
}