import request from '@/utils/request'

// 查询菜单列表
export function listMenu(query) {
  return request({
    url: '/api/menu/menuAndPermTree',
    method: 'get',
    params: query
  })
}

// 查询菜单详细
export function getMenu(menuId) {
  return request({
    url: '/api/menu/getMenu' ,
    method: 'post',
    params: {"id":menuId.toString()}
  })
}

//查询菜单详细
export function getFunction(id) {
  return request({
    url: '/api/permission/get' ,
    method: 'get',
    params: {"id":id.toString()}
  })
}

// 查询菜单下拉树结构
export function treeselect() {
  return request({
    url: '/api/menu/menuAndPermTree',
    method: 'get'
  })
}

// 根据角色ID查询菜单下拉树结构
export function roleMenuTreeselect(roleId) {
  return request({
    url: '/api/menu/menuAndPermTreeSelect/' ,
    method: 'get',
    params: {"id":roleId.toString()}
  })
}

// 新增菜单
export function addMenu(data) {
  return request({
    url: '/api/menu/save',
    method: 'post',
    data: data
  })
}

// 修改菜单
export function updateMenu(data) {
  return request({
    url: '/api/menu/save',
    method: 'post',
    data: data
  })
}

//新增菜单功能
export function addMenuFunction(data) {
  return request({
    url: '/api/permission/save',
    method: 'post',
    data: data
  })
}

// 修改菜单功能
export function updateMenuFunction(data) {
  return request({
    url: '/api/permission/save',
    method: 'post',
    data: data
  })
}



// 删除菜单
export function delMenu(id) {
  var ids=[];
  ids.push(id);

  return request({
    url: '/api/menu/batchDelete',
    method: 'post',
    data:ids
  })
}

//删除菜单功能
export function delFunction(id) {
  var ids=[];
  ids.push(id);

  return request({
    url: '/api/permission/batchDelete',
    method: 'post',
    data:ids
  })
}