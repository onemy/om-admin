import request from '@/utils/request'

// 查询角色列表
export function listRole(query) {
	var pageNum=query.pageNum;
	var pageSize=query.pageSize;
	query.start=(pageNum-1)*pageSize;
	query.length=pageSize;
	
  return request({
    url: '/api/role/list',
    method: 'post',
    params: query
  })
}

// 查询角色详细
export function getRole(roleId) {
  return request({  
    url: '/api/role/getRole/',
    method: 'post',
    params: {"id":roleId.toString()}
  })
}

// 新增角色
export function addRole(data) {
  return request({
    url: '/api/role/save',
    method: 'post',
    data: data
  })
}

// 修改角色
export function updateRole(data) {
  return request({
    url: '/api/role/save',
    method: 'post',
    data: data
  })
}
//修改角色
export function updateRoleMenu(data) {
  return request({
    url: '/api/acl/saveRole',
    method: 'post',
    data: data
  })
}

// 角色数据权限
export function dataScope(data) {
  return request({
    url: '/system/role/dataScope',
    method: 'put',
    data: data
  })
}

// 角色状态修改
export function changeRoleStatus(roleId, status) {
  const data = {
    roleId,
    status
  }
  return request({
    url: '/api/role/changeStatus',
    method: 'post',
    data: data
  })
}

// 删除角色
export function delRole(id) {
  var ids=[];
  if((typeof id=='string') && id.constructor==String){
	  ids.push(id);
  }else{
	  ids=id;
  }
  return request({
    url: '/api/role/batchDelete',
    method: 'post',
    data:ids
  })
}

// 导出角色
export function exportRole(query) {
  return request({
    url: '/system/role/export',
    method: 'get',
    params: query
  })
}