import request from '@/utils/request'
import { praseStrEmpty } from "@/utils/oma";

// 查询用户列表
export function listUser(query) {
	var pageNum=query.pageNum;
	var pageSize=query.pageSize;
	query.start=(pageNum-1)*pageSize;
	query.length=pageSize;
	
  return request({
    url: '/api/user/list',
    method: 'post',
    params: query
  })
}

// 查询用户详细
export function getUser(userId) {
  return request({
    url: '/api/user/' + praseStrEmpty(userId),
    method: 'get'
  })
}

// 新增用户
export function addUser(data) {
  return request({
    url: '/api/user/save',
    method: 'post',
    data: data
  })
}

// 修改用户
export function updateUser(data) {
  return request({
    url: '/api/user/save',
    method: 'post',
    data: data
  })
}

// 删除用户
export function delUser(id) {
  var ids=[];
  if((typeof id=='string') && id.constructor==String){
	  ids.push(id);
  }else{
	  ids=id;
  }
  return request({
    url: '/api/user/batchDelete',
    method: 'post',
    data:ids
  })
}

// 导出用户
export function exportUser(query) {
  return request({
    url: '/system/user/export',
    method: 'get',
    params: query
  })
}

// 用户密码重置
export function resetUserPwd(id, password) {
  const data = {
    id,
    password
  }
  return request({
    url: '/api/user/resetPwd',
    method: 'post',
    data: data
  })
}

// 用户状态修改
export function changeUserStatus(id, status) {
  const data = {
    id,
    status
  }
  return request({
    url: '/api/user/changeStatus',
    method: 'post',
    data: data
  })
}

// 查询用户个人信息
export function getUserProfile() {
  return request({
    url: '/api/user/getInfo',
    method: 'get'
  })
}

// 修改用户个人信息
export function updateUserProfile(data) {
  return request({
    url: '/api/profile/update',
    method: 'post',
    data: data
  })
}

// 用户密码重置
export function updateUserPwd(oldPassword, newPassword) {
  const data = {
    oldPassword,
    newPassword
  }
  return request({
    url: '/api/profile/updatePwd',
    method: 'post',
    params: data
  })
}

// 用户头像上传
export function uploadAvatar(data) {
  return request({
    url: '/system/user/profile/avatar',
    method: 'post',
    data: data
  })
}

// 下载用户导入模板
export function importTemplate() {
  return request({
    url: '/system/user/importTemplate',
    method: 'get'
  })
}
