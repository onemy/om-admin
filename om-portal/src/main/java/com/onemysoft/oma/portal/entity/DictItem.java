package com.onemysoft.oma.portal.entity;

import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.onemysoft.common.entity.UUIDEntity;

/**
 * @author onemysoft
 * 
 */

@Entity
@Table(name = "sys_dict_item")
public class DictItem extends UUIDEntity {
	
	private String itemCode;
	
	private String itemName;
	
	private String itemValue;
	
    /** 状态（0正常 1停用） */
    private String status;

    @OneToOne()
	private Dict dict;

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Dict getDict() {
		return dict;
	}

	public void setDict(Dict dict) {
		this.dict = dict;
	}

	public String getItemValue() {
		return itemValue;
	}

	public void setItemValue(String itemValue) {
		this.itemValue = itemValue;
	}
	
	
	
}
