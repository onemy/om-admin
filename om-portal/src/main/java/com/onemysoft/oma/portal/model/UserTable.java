package com.onemysoft.oma.portal.model;

import java.util.Date;

import com.onemysoft.common.entity.UUIDEntity;

public class UserTable extends UUIDEntity {
	
	
	private String nickname;
	
	private String username;
	
    private String status;
    
    private String mobile;
    
    private Date createDate; 

    

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}




    
    
}
