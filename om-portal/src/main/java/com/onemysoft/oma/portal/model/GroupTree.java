package com.onemysoft.oma.portal.model;

import java.util.Date;

public class GroupTree extends TreeModel<GroupTree> {

	private Integer orderNo;

	private String status;
	
	private Date createDate;

	
	
	public Integer getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(Integer orderNo) {
		this.orderNo = orderNo;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}


	
	
	
}
