package com.onemysoft.oma.portal.entity;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.onemysoft.common.entity.UUIDEntity;

@MappedSuperclass
public abstract class Resource extends UUIDEntity {
	
	@Column(unique = true, nullable = false)
	private String code;
	
	@Column(nullable = false)
	private String name;
	
	private String url;
	
	
    @NotBlank(message = "资源编码不能为空")
    @Size(min = 0, max = 100, message = "资源编码长度不能超过100个字符")
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
    @NotBlank(message = "资源名称不能为空")
    @Size(min = 0, max = 50, message = "资源名称长度不能超过50个字符")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	
	
}
