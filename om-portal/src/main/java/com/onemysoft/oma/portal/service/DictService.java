package com.onemysoft.oma.portal.service;


import java.util.List;

import com.onemysoft.oma.portal.entity.Dict;
import com.onemysoft.oma.portal.entity.DictItem;


/**
 * @author onemysoft
 * 
 */
public interface DictService {

	/**
	 * create dict
	 */
	public Dict createDict(Dict dict);
	
	/**
	 * update dict
	 */
	public Dict updateDict(Dict dict);
	
	/**
	 * 更新状态
	 * @param dict
	 */
	public void updateStatus(Dict dict);
	/**
	 * 
	 * @param id
	 */
	public void deleteDict(String id);
	/**
	 * delete dict
	 */
	public void deleteDicts(String[] ids);
	
	/**
	 * 
	 * @param id
	 * @return
	 */
	public Dict findById(String id);
	

	
	/**
	 * 
	 * @param dictCode
	 * @return
	 */
	public boolean checkDictCodeExist(String dictCode);

	/**
	 * find dicts by dict
	 * @param dict	
	 * @return
	 */
	public List<Dict> findDictList(Dict dict);
	

	/**
	 * find dictItem list by type
	 * @param type
	 * @return
	 */
	public List<DictItem> findDictItemList(String type);
}
