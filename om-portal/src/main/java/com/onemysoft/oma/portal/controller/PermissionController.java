package com.onemysoft.oma.portal.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.util.ObjectUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.onemysoft.common.context.SystemContext;
import com.onemysoft.common.springmvc.intercept.annotation.JsonDataRequestBody;
import com.onemysoft.common.utils.BeanUtils;
import com.onemysoft.common.utils.OMAssert;
import com.onemysoft.common.web.DataTransferObject;
import com.onemysoft.common.web.Result;
import com.onemysoft.oma.portal.entity.Permission;
import com.onemysoft.oma.portal.model.MenuForm;
import com.onemysoft.oma.portal.model.PermissionForm;
import com.onemysoft.oma.portal.model.PermissionTable;
import com.onemysoft.oma.portal.service.PermissionService;

/**
 * @author onemysoft
 * 
 */
@RestController
@RequestMapping("/api/permission")
public class PermissionController {

    @Autowired
    private PermissionService permissionService;
    
    
    @PostMapping("/list")
    @JsonDataRequestBody(start="start", rows="length")
    public Result getPermissionList(@ModelAttribute Permission permission){
    	
    	Page<Permission> list=permissionService.findAll(permission);
    	
    	List<PermissionTable> permissionList=new ArrayList<PermissionTable>();
    	
    	for(Permission p : list) {
    		PermissionTable permissionTable=new PermissionTable();
    		
    		permissionTable.setId(p.getId());
    		permissionTable.setCode(p.getCode());
    		permissionTable.setName(p.getName());
    		permissionTable.setOrderNo(p.getOrderNo());
    		permissionList.add(permissionTable);
    	}
    	
    	DataTransferObject<List<PermissionTable>> dto=new DataTransferObject<List<PermissionTable>>();
    	dto.setDatas(permissionList);
    	dto.setTotalRecordNums(list.getTotalElements());
    	dto.setDraw(SystemContext.getRequestTransferData().getDraw());
    	
        return Result.ok().data(dto);
    }

    @PostMapping("/save")
    public Result save(@Validated @RequestBody Permission perm){
    	
    	if (OMAssert.isNotNull(perm.getId())) {
    		permissionService.updatePermission(perm);
    	}else {
        	if(permissionService.checkPermCodeExist(perm.getCode())) {
        		Result.error().message("该编码己存在！");
        	}
        	permissionService.createPermission(perm);
    	}

        return Result.ok();
    }
    
    @GetMapping("/get")
    public Result getPermissionById(String id){
    	PermissionForm pForm=new PermissionForm();
    	Permission p=permissionService.findById(id);

    	pForm.setId(p.getId());
    	pForm.setDescription(p.getDescription());
    	pForm.setOrderNo(p.getOrderNo());
    	pForm.setCode(p.getCode());
    	pForm.setName(p.getName());
    	pForm.setStatus(p.getStatus());
    	pForm.setUrl(p.getUrl());
    	pForm.setMenuType("P");
    	
    	PermissionForm parent=new PermissionForm();
    	if(!ObjectUtils.isEmpty(p.getMenu())) {
    		parent.setId(p.getMenu().getId());
    		parent.setCode(p.getMenu().getCode());
    		parent.setName(p.getMenu().getName());
    		parent.setStatus(p.getMenu().getStatus());
    		parent.setUrl(p.getMenu().getUrl());
    		parent.setMenuType("P");
    		pForm.setParent(parent);
    	}
    	
        return Result.ok().data(pForm);
    }
    
	/**
	 * 批量删除对象
	 * 
	 * @param id
	 * @return
	 */
    @PostMapping(value = "/batchDelete")
	public Result batchDelete(@RequestBody String[] ids) {
    	this.permissionService.deletePermissions(ids);
    	
		return Result.ok();
	}
    
	/**
	 * 删除对象
	 * 
	 * @param id
	 * @return
	 */
    @PostMapping(value = "/delete")
	public Result delete(String id) {
    	this.permissionService.deletePermission(id);
		return Result.ok();
	}
}
