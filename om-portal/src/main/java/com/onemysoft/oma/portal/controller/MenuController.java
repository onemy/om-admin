package com.onemysoft.oma.portal.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.onemysoft.common.context.SystemContext;
import com.onemysoft.common.utils.OMAssert;
import com.onemysoft.common.web.Result;
import com.onemysoft.oma.portal.entity.Menu;
import com.onemysoft.oma.portal.entity.Permission;
import com.onemysoft.oma.portal.entity.Resource;
import com.onemysoft.oma.portal.entity.Role;
import com.onemysoft.oma.portal.entity.User;
import com.onemysoft.oma.portal.model.GroupTree;
import com.onemysoft.oma.portal.model.MenuForm;
import com.onemysoft.oma.portal.model.MenuMeta;
import com.onemysoft.oma.portal.model.MenuTree;
import com.onemysoft.oma.portal.service.MenuService;
import com.onemysoft.oma.portal.service.PermissionService;
import com.onemysoft.oma.portal.utils.ContextUtils;

/**
 * @author onemysoft
 * 
 */
@RestController
@RequestMapping("/api/menu")
public class MenuController {

    @Autowired
    private MenuService menuService;
    
    @Autowired
    private PermissionService permessionService;
    
    @PostMapping("/tree")
//    @PreAuthorize("hasAnyAuthority('menu/list')")
    public Result getMenuList(@ModelAttribute Menu menu){
    	
    	List<MenuTree> menuTree=new ArrayList<MenuTree>();
		List<Menu> menus=menuService.findMenuList(menu);
		if(!menus.isEmpty()) {
			menuTree=buildMenuTree(menus);
		}
		
        return Result.ok().data(menuTree);
    }
    
    @PostMapping("/userTree")
//  @PreAuthorize("hasAnyAuthority('menu/list')")
	public Result getMenuList(){
    	List<MenuTree> menuTree=new ArrayList<MenuTree>();
    	String username=ContextUtils.getCurrentUserName();
    	
    	if(User.isAdmin(username)) {
    		List<Menu> menus=menuService.findMenuList(new Menu());
    		if(!menus.isEmpty()) {
    			menuTree=buildMenuTree(menus);
    		}
    	}else {
    		List<Menu> menus=menuService.findACLByUser(username);
    		if(!menus.isEmpty()) {
    			menuTree=buildMenuTree(menus);
    		}
    	}
    	return Result.ok().data(menuTree);
	}
    
    /**
     * 返回所有菜单及功能的组合（一棵树形）
     * 针对vue方法
     * @return
     */
    @GetMapping("/menuPermTree")
	public Result getMenuPermTree(Menu menu){
    	List<MenuTree> menuTrees=new ArrayList<MenuTree>();
    	String username=ContextUtils.getCurrentUserName();
    	
    	Permission p=new Permission();
    	p.setName(menu.getName());
    	p.setStatus(menu.getStatus());
    	
    	List<Permission> permissions=permessionService.findPermissionList(p);

    	
    	if(User.isAdmin(username)) {
    		List<Menu> menus=menuService.findMenuList(menu);
    		
	    	for(Permission perm:permissions) {
	    		Menu m=new Menu();
	    		m.setId(perm.getId());
	    		m.setParent(perm.getMenu());
	    		m.setCode(perm.getCode());
	    		m.setName(perm.getName());
	    		menus.add(m);
	    	}
	    	
    		if(!menus.isEmpty()) {

    			menuTrees=buildMenuTree(menus);
    		}
    	}else {
    		List<Menu> menus=menuService.findACLByUser(username);
    		
	    	for(Permission perm:permissions) {
	    		Menu m=new Menu();
	    		m.setId(perm.getId());
	    		m.setParent(perm.getMenu());
	    		m.setCode(perm.getCode());
	    		m.setName(perm.getName());
	    		menus.add(m);
	    	}
	    	
    		if(!menus.isEmpty()) {

    			menuTrees=buildMenuTree(menus);
    		}
    	}
    	return Result.ok().data(menuTrees);
	}
    /**
     * 组合菜单及功能为一个树模型
     * 针对vue方法
     * @return
     */
    @GetMapping("/menuAndPermTree")
	public Result getMenuAndPermTree(){
    	
    	String username=ContextUtils.getCurrentUserName();
    	List<MenuTree> menuTrees=new ArrayList<MenuTree>();
    	List<Resource> resources=new ArrayList<Resource>();
    	List<Permission> permissions=permessionService.findAll();
    	resources.addAll((Collection<? extends Resource>) permissions);
    	
    	if(User.isAdmin(username)) {
    		List<Menu> menus=menuService.findMenuList(new Menu());
    		if(!menus.isEmpty()) {
    			resources.addAll((Collection<? extends Resource>) menus);
    			menuTrees=buildResourceTree(resources);
    		}
    	}else {
    		List<Menu> menus=menuService.findACLByUser(username);
    		if(!menus.isEmpty()) {
    			resources.addAll((Collection<? extends Resource>) menus);
    			menuTrees=buildResourceTree(resources);
    		}
    	}
    	return Result.ok().data(menuTrees);
	}
    
    /**
     * 组合菜单及功能为一个树模型及所选中
     * 针对vue方法
     * @return
     */
    @GetMapping("/menuAndPermTreeSelect")
	public Result menuAndPermTreeSelect(Role role){
    	//定义返回数据
      	Map<String, Object> map =new HashMap<String, Object>();
    	
    	map.put("menus", getMenuAndPermTree().getData());
    	
      	//处理菜单，查询菜单，及有权菜单
        List<Menu> menus=menuService.findACLByRole(role);
        List<Permission> perms=permessionService.findPermissionsByRole(role);
        
        List<String> checkedKeys=new ArrayList();
        for(Menu m : menus) {
        	checkedKeys.add(m.getId());
        }
        for(Permission p : perms) {
        	checkedKeys.add(p.getId());
        }
        
        map.put("checkedKeys", checkedKeys);
        
        
    	return Result.ok().data(map);
	}
    
    /**
     * 返回所有菜单及功能的组合（及所选中）
     * 针对vue方法
     * @return
     */
    @PostMapping("/menuPermTreeAndSelect")
	public Result menuPermTreeAndSelect(Role role){
    	
    	//定义返回数据
      	Map<String, Object> map =new HashMap<String, Object>();
      	
      	
    	List<MenuTree> menuTrees=new ArrayList<MenuTree>();
    	String username=ContextUtils.getCurrentUserName();
    	
    	List<Permission> permissions=permessionService.findAll();

    	
    	if(User.isAdmin(username)) {
    		List<Menu> menus=menuService.findMenuList(new Menu());
    		if(!menus.isEmpty()) {
    	    	for(Permission perm:permissions) {
    	    		Menu m=new Menu();
    	    		m.setId(perm.getId());
    	    		m.setParent(perm.getMenu());
    	    		m.setCode(perm.getCode());
    	    		m.setName(perm.getName());
    	    		menus.add(m);
    	    	}
    			menuTrees=buildMenuTree(menus);
    		}
    	}else {
    		List<Menu> menus=menuService.findACLByUser(username);
    		if(!menus.isEmpty()) {
    	    	for(Permission perm:permissions) {
    	    		Menu m=new Menu();
    	    		m.setId(perm.getId());
    	    		m.setParent(perm.getMenu());
    	    		m.setCode(perm.getCode());
    	    		m.setName(perm.getName());
    	    		menus.add(m);
    	    	}
    			menuTrees=buildMenuTree(menus);
    		}
    	}
    	
    	map.put("menus", menuTrees);
    	
    	/**
    	 * 处理己选菜单及功能
    	 */
    	
      	//处理菜单，查询菜单，及有权菜单
        List<Menu> menus=menuService.findACLByRole(role);
        List<Permission> perms=permessionService.findPermissionsByRole(role);
        
        List<String> checkedKeys=new ArrayList();
        for(Menu m : menus) {
        	checkedKeys.add(m.getId());
        }
        
        for(Permission p : perms) {
        	checkedKeys.add(p.getId());
        }
        
        map.put("checkedKeys", checkedKeys);
    	
    	return Result.ok().data(map);
	}
    
    /**
     * 构建树形方法
     * @param resources
     * @return
     */
    private List<MenuTree> buildResourceTree(List<Resource> resources){
		// 递归树形结构
		List<MenuTree> mtList = new ArrayList<MenuTree>();
		Map<String, Object> menuList = new HashMap<String, Object>();
		
		for (Resource r : resources) {
			MenuTree mt = new MenuTree();
			mt.setId(r.getId());
			mt.setCode(r.getCode());
			mt.setName(r.getName());
			mt.setOrderNo(r.getOrderNo());
			mt.setCreateDate(r.getCreateDate());
			
			if(r instanceof Menu) {
				mt.setMenuType("M");
				Menu d=(Menu)r;
				mt.setStatus(d.getStatus());
				
				if (d.getParent() != null) {
					mt.setPid(d.getParent().getId());
					MenuTree parentV = new MenuTree();
					parentV.setId(d.getParent().getId());
					parentV.setCode(d.getParent().getCode());
					parentV.setName(d.getParent().getName());
					parentV.setOrderNo(d.getParent().getOrderNo());
					parentV.setMenuType("M");
					mt.setParent(parentV);
				}
			}else if(r instanceof Permission){
				mt.setMenuType("P");
				Permission d=(Permission)r;
				mt.setStatus(d.getStatus());
				
				if (d.getMenu() != null) {
					mt.setPid(d.getMenu().getId());
					MenuTree parentV = new MenuTree();
					parentV.setId(d.getMenu().getId());
					parentV.setCode(d.getMenu().getCode());
					parentV.setName(d.getMenu().getName());
					parentV.setOrderNo(d.getMenu().getOrderNo());
					parentV.setMenuType("P");
					mt.setParent(parentV);
				}
			}

			menuList.put(mt.getId(), mt);
			mtList.add(mt);
		}

		List<MenuTree> root = new ArrayList<MenuTree>();
		Set entrySet = menuList.entrySet();
		
		for (Iterator it = entrySet.iterator(); it.hasNext();) {
			@SuppressWarnings("unchecked")
			MenuTree menuVo = (MenuTree) ((Map.Entry<String, Object>) it.next()).getValue();
			
			if(menuVo.getParent()==null) continue;
			
			if (menuVo.getParent().getId().equals("0")) {
				root.add(menuVo);
			} else {
				MenuTree v = (MenuTree) menuList.get(menuVo.getParent().getId());
				v.getChildren().add(menuVo);
			}
		}


		return root;
    }
    
    /**
     * 构建树形方法
     * @param menus
     * @return
     */
    private List<MenuTree> buildMenuTree(List<Menu> menus){
		// 递归树形结构
		List<MenuTree> mtList = new ArrayList<MenuTree>();
		Map<String, Object> menuList = new HashMap<String, Object>();
		for (Menu d : menus) {
			MenuTree mt = new MenuTree();
			MenuMeta mm = new MenuMeta();
			mm.setTitle(d.getName());
			mm.setIcon(d.getIcon_svg());
			mt.setMeta(mm);
			mt.setId(d.getId());
			mt.setCode(d.getCode());
			mt.setIcon(d.getIcon());
			mt.setName(d.getName());
			mt.setUrl(d.getUrl());
			mt.setPath(d.getPath());
			mt.setComponent(d.getComponent());
			mt.setOrderNo(d.getOrderNo());
			mt.setStatus(d.getStatus());
			mt.setMenuType("M");
			if (d.getParent() != null) {
				mt.setPid(d.getParent().getId());
				MenuTree parentV = new MenuTree();
				parentV.setId(d.getParent().getId());
				parentV.setCode(d.getParent().getCode());
				parentV.setIcon(d.getParent().getIcon());
				parentV.setName(d.getParent().getName());
				parentV.setUrl(d.getParent().getTarget());
				parentV.setOrderNo(d.getParent().getOrderNo());
				parentV.setStatus(d.getStatus());
				parentV.setMenuType("M");
				mt.setParent(parentV);
			}
			menuList.put(mt.getId(), mt);
			mtList.add(mt);
		}

		List<MenuTree> root = new ArrayList<MenuTree>();
		Set entrySet = menuList.entrySet();
		
		for (Iterator it = entrySet.iterator(); it.hasNext();) {
			@SuppressWarnings("unchecked")
			MenuTree menuVo = (MenuTree) ((Map.Entry<String, Object>) it.next()).getValue();
			
			if(menuVo.getParent()==null) continue;
			
			if (menuVo.getParent().getId().equals("0")) {
				root.add(menuVo);
			} else {
				MenuTree v = (MenuTree) menuList.get(menuVo.getParent().getId());

				if(v!=null) {
					v.getChildren().add(menuVo);
				}else {
					root.add(menuVo);
				}
			}
		}


		return root;
    }
    
    /**
     * 获取所有菜单
     * @param menu
     * @return
     */
    @PostMapping("/all")
//    @PreAuthorize("hasAnyAuthority('menu/list')")
    public Result getAll(@ModelAttribute Menu menu){
    	
    	List<Menu> list=menuService.findMenuList(menu);
    	List<Map<String, String>> tree = new ArrayList<Map<String, String>>();
    	for(Menu g:list) {
    		Map<String, String> m = new HashMap<String, String>();
			m.put("id", g.getId());
			m.put("pid", g.getParent() == null ? "-1" : g.getParent().getId());
			m.put("name", g.getName());
    		
			tree.add(m);
    	}
    	
        return Result.ok().data(tree);
    }
    
    
    @PostMapping("/userMenus")
//  @PreAuthorize("hasAnyAuthority('menu/list')")
  public Result getUserMenus(String username){
    
  	//定义返回数据
  	Map<String, Object> map =new HashMap<String, Object>();
  	List<Map<String, Object>> menu = new ArrayList<Map<String, Object>>();
  	List<Map<String, Object>> permission = new ArrayList<Map<String, Object>>();
  	
  	//处理菜单，查询菜单，及有权菜单
    List<Menu> menus=menuService.findACLByUser(username);
    
  	List<Menu> allMenus=menuService.findMenuList(new Menu());
  	
  	for(Menu g : allMenus) {
  		Map<String, Object> m = new HashMap<String, Object>();
			m.put("id", g.getId());
			m.put("pid", g.getParent() == null ? "-1" : g.getParent().getId());
			m.put("name", g.getName());
			if(menus.contains(g)) {
				m.put("checked", true);
			}
			
			menu.add(m);
  	}
  	
  	//处理功能
  	
  	List<Permission> permissions=permessionService.findPermissionsByUsername(username);
  	List<Permission> allPermissions=permessionService.findAll();
  	
  	for(Permission p : allPermissions) {
  		Map<String, Object> m = new HashMap<String, Object>();
  		m.put("id", p.getId());
  		m.put("name", p.getName());
  		m.put("menuId", p.getMenu().getId());
		if(permissions.contains(p)) {
			m.put("checked", true);
		}else {
			m.put("checked", false);
		}
		permission.add(m);
  	}
  	
  	
  	//构建返回map数据
  	map.put("menus", menu);
  	map.put("permissions", permission);
  	
	return Result.ok().data(map);
  }


    @PostMapping("/roleMenus")
//  @PreAuthorize("hasAnyAuthority('menu/list')")
  public Result getRoleMenus(Role role){
    
  	//定义返回数据
  	Map<String, Object> map =new HashMap<String, Object>();
  	List<Map<String, Object>> menu = new ArrayList<Map<String, Object>>();
  	List<Map<String, Object>> permission = new ArrayList<Map<String, Object>>();
  	
  	//处理菜单，查询菜单，及有权菜单
    List<Menu> menus=menuService.findACLByRole(role);
    
  	List<Menu> allMenus=menuService.findMenuList(new Menu());
  	
  	for(Menu g : allMenus) {
  		Map<String, Object> m = new HashMap<String, Object>();
			m.put("id", g.getId());
			m.put("pid", g.getParent() == null ? "-1" : g.getParent().getId());
			m.put("name", g.getName());
			if(menus.contains(g)) {
				m.put("checked", true);
			}
			
			menu.add(m);
  	}
  	
  	//处理功能
  	
  	List<Permission> permissions=permessionService.findPermissionsByRole(role);
  	List<Permission> allPermissions=permessionService.findAll();
  	
  	for(Permission p : allPermissions) {
  		Map<String, Object> m = new HashMap<String, Object>();
  		m.put("id", p.getId());
  		m.put("name", p.getName());
  		m.put("menuId", p.getMenu().getId());
		if(permissions.contains(p)) {
			m.put("checked", true);
		}else {
			m.put("checked", false);
		}
		permission.add(m);
  	}
  	
  	
  	//构建返回map数据
  	map.put("menus", menu);
  	map.put("permissions", permission);
  	
	return Result.ok().data(map);
  }
    
    
    
    @PostMapping("/save")
    public Result save(@Validated @RequestBody Menu menu){
    	
    	if (OMAssert.isNotNull(menu.getId())) {
    		menuService.updateMenu(menu);
    	}else {
        	if(menuService.checkMenuCodeExist(menu.getCode())) {
        		Result.error().message("该编码己存在！");
        	}
        	menuService.createMenu(menu);
    	}

        return Result.ok();
    }
    
    @PostMapping("/getMenu")
    public Result getMenuById(String id){
    	MenuForm menuForm=new MenuForm();
    	Menu menu=menuService.findById(id);
    	
//    	BeanUtils.copyProperties(menu,menuForm);
    	
    	menuForm.setId(menu.getId());
    	menuForm.setDescription(menu.getDescription());
    	menuForm.setOrderNo(menu.getOrderNo());
    	menuForm.setCode(menu.getCode());
    	menuForm.setName(menu.getName());
    	menuForm.setStatus(menu.getStatus());
    	menuForm.setIcon(menu.getIcon());
    	menuForm.setUrl(menu.getUrl());
    	menuForm.setPath(menu.getPath());
    	menuForm.setTarget(menu.getTarget());
    	menuForm.setComponent(menu.getComponent());
    	menuForm.setMenuType("M");
    	
    	MenuForm parentMenu=new MenuForm();
    	if(!ObjectUtils.isEmpty(menu.getParent())) {
	    	parentMenu.setId(menu.getParent().getId());
	    	parentMenu.setCode(menu.getParent().getCode());
	    	parentMenu.setName(menu.getParent().getName());
	    	parentMenu.setStatus(menu.getParent().getStatus());
	    	parentMenu.setIcon(menu.getParent().getIcon());
	    	parentMenu.setUrl(menu.getParent().getUrl());
	    	parentMenu.setPath(menu.getParent().getPath());
	    	parentMenu.setTarget(menu.getTarget());
	    	parentMenu.setComponent(menu.getParent().getComponent());
	    	parentMenu.setMenuType("M");
	    	menuForm.setParent(parentMenu);
    	}
    	
        return Result.ok().data(menuForm);
    }
    
    
	/**
	 * 批量删除对象
	 * 
	 * @param id
	 * @return
	 */
    @PostMapping(value = "/batchDelete")
	public Result batchDelete(@RequestBody String[] ids) {
    	this.menuService.deleteMenus(ids);
    	
		return Result.ok();
	}
    
	/**
	 * 删除对象
	 * 
	 * @param id
	 * @return
	 */
    @PostMapping(value = "/delete")
	public Result delete(String id) {
    	this.menuService.deleteMenu(id);
		return Result.ok();
	}
    
    /**
     * 校验角色编码
     */
    @PostMapping("/checkMenuCodeExsit")
    @ResponseBody
    public Result checkUsernameExsit(String menuCode) {
    	if(menuService.checkMenuCodeExist(menuCode)) {
    		return Result.error();
    	}else {
    		return Result.ok();
    	}
    }
    
    @PostMapping("/changeStatus")
    public Result disableRole(@RequestBody Menu menu){
    	
    	if (OMAssert.isNotNull(menu.getId())) {
    		menuService.updateStatus(menu);
    	}

        return Result.ok();
    }
}
