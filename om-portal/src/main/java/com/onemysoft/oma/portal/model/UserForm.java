package com.onemysoft.oma.portal.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import com.onemysoft.common.entity.UUIDEntity;
import com.onemysoft.oma.portal.entity.Group;
import com.onemysoft.oma.portal.entity.Role;

/**
 * @author onemysoft
 * 
 */

public class UserForm extends UUIDEntity {
	
    private String username;
    
    private String nickname;

    private String password;
    
    private Date startDate;
    
    private Date endDate;
    
    private String type;

    private String email;
    
    private String mobile;
    
    private String status;
    
    private String sex;
    
	private String pic;
	

    private List<Role> roles = new ArrayList<Role>();
    
    private String[] roleIds;

    private Group group ;
    
    

	public String[] getRoleIds() {
		return roleIds;
	}

	public void setRoleIds(String[] roleIds) {
		this.roleIds = roleIds;
	}

	public Group getGroup() {
		return group;
	}

	public void setGroup(Group group) {
		this.group = group;
	}

	public List<Role> getRoles() {
		return roles;
	}

	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}

	public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getPic() {
		return pic;
	}

	public void setPic(String pic) {
		this.pic = pic;
	}


	@Override
    public String toString() {
        return "User{" +
                "userId=" + super.getId() +
                ", userName='" + username + '\'' +
                ", roles=" + roles +
                '}';
    }


}
