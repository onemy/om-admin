package com.onemysoft.oma.portal.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import com.onemysoft.common.context.SystemContext;
import com.onemysoft.common.utils.BeanUtils;
import com.onemysoft.common.utils.OMAssert;
import com.onemysoft.oma.portal.entity.Group;
import com.onemysoft.oma.portal.entity.Menu;
import com.onemysoft.oma.portal.entity.Permission;
import com.onemysoft.oma.portal.entity.Role;
import com.onemysoft.oma.portal.exception.ResourceNotFoundException;
import com.onemysoft.oma.portal.repository.AclRepository;
import com.onemysoft.oma.portal.repository.PermissionRepository;
import com.onemysoft.oma.portal.repository.ResourceRepository;
import com.onemysoft.oma.portal.service.PermissionService;

@Service
public class PermissionServiceImpl implements PermissionService {

    @Autowired
    private ResourceRepository resourceRepository;
    
    @Autowired
    private PermissionRepository permissionRepository;
    
    @Autowired
    private AclRepository aclRepository;
    
	@Override
	public Permission createPermission(Permission p) {
    	String currentUser=(String) SystemContext.getUserMap().get("username");
    	p.setCreator(currentUser);
    	p.setCreateDate(new Date());
    	p.setUpdateDate(new Date());
        
		return permissionRepository.save(p);
	}

	@Override
	public Permission updatePermission(Permission p) {
        Optional < Permission > permDb = this.permissionRepository.findById(p.getId());

        if (permDb.isPresent()) {
        	Permission pUpdate = permDb.get();

        	BeanUtils.copyProperties(p, pUpdate);
        	String currentUser=(String) SystemContext.getUserMap().get("groupname");
            p.setUpdater(currentUser);
            p.setUpdateDate(new Date());
            
            permissionRepository.save(pUpdate);
            
            return pUpdate;
        } else {
            throw new ResourceNotFoundException("记录没找到 id : " + p.getId());
        }
	}



	@Override
	public List<Permission> findPermissionsByUsername(String username) {

		return resourceRepository.findPermissionsByUser(username);
	}

	@Override
	public Page<Permission> findAll(Permission permission) {
		
		int start = SystemContext.getRequestTransferData() == null ? 0 : SystemContext.getRequestTransferData().getStart();
		int limit = SystemContext.getRequestTransferData() == null ? Integer.MAX_VALUE : SystemContext.getRequestTransferData().getRows();
		
    	Sort sort = Sort.by(Direction.ASC, "orderNo");
    	Pageable pageable = PageRequest.of((start/limit), limit, sort);
    	
    	Example<Permission> ex=Example.of(permission);
    	
    	Page<Permission> list=permissionRepository.findAll(ex,pageable);
    	
		return list;
	}

	@Override
	public List<Permission> findPermissionList(Permission p) {
    	ExampleMatcher matcher = ExampleMatcher.matching()
                .withMatcher("name", ExampleMatcher.GenericPropertyMatchers.contains());//模糊查询匹配开头，即%{name}%

    	Example<Permission> ex=Example.of(p,matcher);
    	
    	List<Permission> list=permissionRepository.findAll(ex);

		return list;
	}

	@Override
	public List<Permission> findPermissionsByRole(Role role) {
		return aclRepository.findPermissionByPrincipal(role);
	}

	@Override
	public boolean checkPermCodeExist(String code) {
		Permission perm=permissionRepository.findByCode(code);
		
		if(ObjectUtils.isEmpty(perm)) {
			return false;
		}else {
			return true;
		}
	}

	@Override
	public List<Permission> findAll() {
		// TODO Auto-generated method stub
		return permissionRepository.findAll();
	}

	@Override
	public Permission findById(String id) {
		Optional < Permission > permissionDb = this.permissionRepository.findById(id);
		Permission p=permissionDb.get();
		return p;
	}

	@Override
	@Transactional
	public void deletePermissions(String[] ids) {
		OMAssert.notNull(ids, "exception", "PermissionId is null");
		
		List<String> list=new ArrayList<>();
		Collections.addAll(list,ids);
		
		permissionRepository.deleteByIdIn(list);
	}
	
	@Override
	@Transactional
	public void deletePermission(String id) {
        Optional < Permission > menuDb = this.permissionRepository.findById(id);

        if (menuDb.isPresent()) {
            this.permissionRepository.delete(menuDb.get());
        } else {
            throw new ResourceNotFoundException("记录没找到 id : " + id);
        }
	}
	

}
