package com.onemysoft.oma.portal.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.onemysoft.oma.portal.entity.Dict;
import com.onemysoft.oma.portal.entity.DictItem;
import com.onemysoft.oma.portal.mapper.DictMapper;
import com.onemysoft.oma.portal.service.DictService;

@Service
public class DictServiceImpl implements DictService {

    @Autowired
    private DictMapper dictMapper;
    
	@Override
	public Dict createDict(Dict dict) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Dict updateDict(Dict dict) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void updateStatus(Dict dict) {
		// TODO Auto-generated method stub

	}

	@Override
	public void deleteDict(String id) {
		// TODO Auto-generated method stub

	}

	@Override
	public void deleteDicts(String[] ids) {
		// TODO Auto-generated method stub

	}

	@Override
	public Dict findById(String id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean checkDictCodeExist(String dictCode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List<Dict> findDictList(Dict dict) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<DictItem> findDictItemList(String type) {

		return dictMapper.findDictByType(type);
	}

}
