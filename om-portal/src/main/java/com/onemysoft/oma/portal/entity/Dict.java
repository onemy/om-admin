package com.onemysoft.oma.portal.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.onemysoft.common.entity.UUIDEntity;

/**
 * @author onemysoft
 * 
 */

@Entity
@Table(name = "sys_dict")
public class Dict extends UUIDEntity {
	
	private String dictCode;
	
	private String dictName;

    /** 状态（0正常 1停用） */
    private String status;

	public String getDictCode() {
		return dictCode;
	}

	public void setDictCode(String dictCode) {
		this.dictCode = dictCode;
	}

	public String getDictName() {
		return dictName;
	}

	public void setDictName(String dictName) {
		this.dictName = dictName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
    
    
	
}
