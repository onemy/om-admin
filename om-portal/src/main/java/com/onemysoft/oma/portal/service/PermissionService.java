package com.onemysoft.oma.portal.service;


import java.util.List;

import org.springframework.data.domain.Page;

import com.onemysoft.oma.portal.entity.Permission;
import com.onemysoft.oma.portal.entity.Role;


/**
 * @author onemysoft
 * 
 */
public interface PermissionService {

	/**
	 * save Permission
	 * @param id
	 * @return
	 */
	public Permission createPermission(Permission p);

	/**
	 * update Permission
	 * @param id
	 * @return
	 */
	public Permission updatePermission(Permission p);

	/**
	 * delete Permission
	 * @param id
	 * @return
	 */
	public void deletePermission(String id);
	
	public void deletePermissions(String[] ids);
	
	public Permission findById(String id);

	public boolean checkPermCodeExist(String code);

	public List<Permission> findPermissionsByUsername(String username);
	
	public List<Permission> findPermissionsByRole(Role role);
	
	public List<Permission> findPermissionList(Permission permission);
	
	public List<Permission> findAll();
	
	public Page<Permission> findAll(Permission permission);
	
}
