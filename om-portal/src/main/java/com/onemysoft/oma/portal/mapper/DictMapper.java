package com.onemysoft.oma.portal.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.onemysoft.oma.portal.entity.DictItem;


/**
 * 字典访问接口
 * @author onemysoft
 * 
 */
@Mapper
public interface DictMapper {
	
	
	@Select("SELECT b.item_code ItemCode,b.item_name itemName,b.item_value itemValue,b.status FROM sys_dict a inner join sys_dict_item b on a.id=b.dict_id and a.dict_code=#{type}")
	List<DictItem> findDictByType(@Param(value = "type") String type);
}
