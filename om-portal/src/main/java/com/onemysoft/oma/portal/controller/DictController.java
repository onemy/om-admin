package com.onemysoft.oma.portal.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.onemysoft.common.utils.OMAssert;
import com.onemysoft.common.web.Result;
import com.onemysoft.oma.portal.entity.Dict;
import com.onemysoft.oma.portal.entity.DictItem;
import com.onemysoft.oma.portal.service.DictService;

/**
 * @author onemysoft
 * 
 */
@RestController
@RequestMapping("/api/dict")
public class DictController {

    @Autowired
    private DictService dictService;
    
    
    
    @GetMapping("/item/{type}")
//    @PreAuthorize("hasAnyAuthority('dict/list')")
    public Result getDictList(@Validated @PathVariable String type){

    	List<DictItem> list=dictService.findDictItemList(type);
    	if(!CollectionUtils.isEmpty(list)) {
    		return Result.ok().data(list);
    	}
        return Result.error();
    }


    
    @PostMapping("/save")
    public Result save(Dict dict){
    	
    	if (OMAssert.isNotNull(dict.getId())) {
    		dictService.updateDict(dict);
    	}else {
        	if(dictService.checkDictCodeExist(dict.getDictCode())) {
        		Result.error().message("该帐号己存在！");
        	}
        	dictService.createDict(dict);
    	}

        return Result.ok();
    }
    
    
	/**
	 * 批量删除对象
	 * 
	 * @param id
	 * @return
	 */
    @PostMapping(value = "/batchDelete")
	public Result batchDelete(@RequestParam(value="ids[]") String[] ids) {
    	this.dictService.deleteDicts(ids);
    	
		return Result.ok();
	}
    
	/**
	 * 删除对象
	 * 
	 * @param id
	 * @return
	 */
    @PostMapping(value = "/delete")
	public Result delete(String id) {
    	this.dictService.deleteDict(id);
		return Result.ok();
	}
    
    /**
     * 校验角色编码
     */
    @PostMapping("/checkDictCodeExsit")
    @ResponseBody
    public Result checkUsernameExsit(String dictCode) {
    	if(dictService.checkDictCodeExist(dictCode)) {
    		return Result.error();
    	}else {
    		return Result.ok();
    	}
    }

    @PostMapping("/disable")
    public Result disableRole(@Validated @RequestBody Dict dict){
    	
    	if (OMAssert.isNotNull(dict.getId())) {
    		dictService.updateStatus(dict);
    	}

        return Result.ok();
    }
}
