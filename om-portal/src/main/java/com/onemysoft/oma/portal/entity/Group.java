package com.onemysoft.oma.portal.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.onemysoft.common.entity.UUIDEntity;



@Entity
@Table(name = "sys_group")
@JsonIdentityInfo(
		  generator = ObjectIdGenerators.PropertyGenerator.class, 
		  property = "id")
public class Group extends UUIDEntity implements Principal {
	
	
	private String groupCode;
	
	private String groupName;
	
    /** 状态（0正常 1停用） */
    private String status;
    
	@OneToOne()
	private Group parent;
	
	@OneToMany(mappedBy = "parent")
	private Set<Group> children = new HashSet<Group>();
	
		
    @NotBlank(message = "部门编码不能为空")
    @Size(min = 0, max = 100, message = "部门编码长度不能超过100个字符")
	public String getGroupCode() {
		return groupCode;
	}

	public void setGroupCode(String groupCode) {
		this.groupCode = groupCode;
	}

    @NotBlank(message = "部门名称不能为空")
    @Size(min = 0, max = 30, message = "部门名称长度不能超过50个字符")
	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Group getParent() {
		return parent;
	}

	public void setParent(Group parent) {
		this.parent = parent;
	}

	public Set<Group> getChildren() {
		return children;
	}

	public void setChildren(Set<Group> children) {
		this.children = children;
	}



	
	
}
